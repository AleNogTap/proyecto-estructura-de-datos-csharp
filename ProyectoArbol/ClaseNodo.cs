﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoArbol
{
    class ClaseNodo
    {
        private string dato;
        private ClaseNodo hijo;
        private ClaseNodo hermano;

        public string Dato
        {
            get { return dato; }
            set { dato = value; }
        }
        public ClaseNodo Hijo
        {
            get { return hijo; }
            set { hijo = value; }
        }
        public ClaseNodo Hermano
        {
            get { return hermano; }
            set { hermano = value; }
        }
        public ClaseNodo()
        {
            dato = "";
            hijo = null;
            hermano = null;
        }
    }
}
