﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProyectoArbol
{
    class Program
    {
        static void Main(string[] args)
        {
            Menú();
        }
        public static void Menú()
        {
            int n = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("Proyecto Estructura de Datos" + "\n" + "Alejandro Nogales Tapia" + "\n" + "Implementación de Árboles" + "\n");
                Console.WriteLine("1. Descripción del proyecto" + "\n" + "2. Iniciar Árbol" + "\n" + "3. Salir");
                Console.Write("\n" + "Seleccione opción: ");
                n = int.Parse(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        Console.Clear();
                        Texto("Descripción.txt");
                        Console.WriteLine("\n" + "Presione cualquier tecla para volver al menú");
                        Console.ReadKey();
                        Menú();
                        break;
                    case 2:
                        Console.Clear();
                        ClaseArbol arbol = new ClaseArbol();
                        Console.WriteLine("Inserte Nombre de su Horario:");
                        string a = Console.ReadLine();
                        ClaseNodo raiz = arbol.Insertar(a, null);
                        MenúArbol(arbol,raiz);
                        Console.WriteLine("\n" + "Presione cualquier tecla para volver al menú");
                        Console.ReadKey();
                        Menú();
                        break;
                    case 3:
                        return;
                    default:
                        Console.Clear();
                        Console.WriteLine("No se ha seleccionado ninguna opción" + "\n" + "Presione cualquier tecla para volver al menú principal");
                        Console.ReadKey();
                        Menú();
                        break;
                }
                Console.ReadKey();
            } while (n == 3);
        }
        public static void MenúArbol(ClaseArbol darbol, ClaseNodo draiz)
        {
            ClaseArbol arbol = darbol;
            ClaseNodo raiz = draiz;
            ClaseNodo encontrado;
            int m = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("Qué desea hacer con el árbol?");
                Console.WriteLine("1. Introducir Nodos Hijos o Hermanos" + "\n" + "2. Mostrar/Guardar Horario Árbol" + "\n" + "3. Buscar dato de Árbol" + "\n" + "4. Mostrar/Guardar sub-árbol día" + "\n" + "5. Volver al menú anterior" + "\n" + "5. Salir");
                Console.Write("\n" + "Seleccione opción: ");
                m = int.Parse(Console.ReadLine());
                switch (m)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Ingrese datos:");
                        string donde = "";
                        string que = "";
                        Console.WriteLine("Deseas insertar días o materias (D/M):");
                        string dm = Console.ReadLine();
                        if (dm.ToUpper() == "D")
                        {
                            donde = raiz.Dato;
                        }
                        else
                        {
                            Console.Write("En que día deseas insertar: ");
                            donde = Console.ReadLine();
                        }
                        Console.Write("Que deseas insertar: ");
                        que = Console.ReadLine();
                        encontrado = arbol.Buscar(donde, raiz);
                        arbol.Insertar(que, encontrado);
                        Console.WriteLine("\n" + "Presione cualquier tecla para volver al menú anterior");
                        Console.ReadKey();
                        MenúArbol(arbol, raiz);
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Su Horario es:");
                        arbol.TransversalPreorden(raiz,raiz.Dato);
                        Console.WriteLine("Se creo un archivo llamado " + raiz.Dato+ ".txt en su capeta debug");
                        Console.WriteLine("Presione cualquier tecla para volver al menú anterior");
                        Console.ReadKey();
                        MenúArbol(arbol, raiz);
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Que desea buscar: ");
                        string busqueda1 = Console.ReadLine();
                        encontrado = arbol.Buscar(busqueda1, raiz);
                        if (encontrado != null)
                        {
                            Console.WriteLine("Su busqueda es:" + encontrado.Dato);
                        }
                        else
                        {
                            Console.WriteLine("No se encontró el dato");
                        }
                        Console.WriteLine("\n" + "Presione cualquier tecla para volver al menú anterior");
                        Console.ReadKey();
                        MenúArbol(arbol, raiz);
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Que desea buscar/imprimir: ");
                        string busqueda2 = Console.ReadLine();
                        encontrado = arbol.Buscar(busqueda2, raiz);
                        if (encontrado != null)
                        {
                            Console.WriteLine("Su busqueda es:" + encontrado.Dato);
                            Console.WriteLine("Se creo un archivo llamado " + encontrado.Dato + ".txt en su capeta debug");
                            arbol.TransversalPreorden(encontrado, encontrado.Dato);
                        }
                        else
                        {
                            Console.WriteLine("No se encontró el dato");
                        }
                        Console.WriteLine("\n" + "Presione cualquier tecla para volver al menú anterior");
                        Console.ReadKey();
                        MenúArbol(arbol, raiz);
                        break;
                    case 5:
                        Console.Clear();
                        Menú();
                        break;
                    case 6:
                        return;
                    default:
                        Console.Clear();
                        Console.WriteLine("No se ha seleccionado ninguna opción" + "\n" + "Presione cualquier tecla para volver al menú anterior");
                        Console.ReadKey();
                        MenúArbol(arbol,raiz);
                        break;
                }
                Console.ReadKey();
            } while (m == 6); 
        }
        public static void Texto(string a)
        {
            string line = "";
            string archivo = Path.GetFullPath(a);
            StreamReader sr = new StreamReader(archivo);
            line = sr.ReadLine();
            while (line != null)
            {
                Console.WriteLine(line);
                line = sr.ReadLine();
            }
            sr.Close();
            Console.ReadKey();
        }
    }
}
