﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProyectoArbol
{
    class ClaseArbol
    {
        private ClaseNodo raiz;
        private ClaseNodo aux;
        private int i = 0;

        public ClaseArbol()
        {
            raiz = new ClaseNodo();
        }
        public ClaseNodo Insertar(string pDato, ClaseNodo pNodo)
        {
            if (pNodo == null) 
            {
                raiz = new ClaseNodo();
                raiz.Dato = pDato;
                raiz.Hijo = null;
                raiz.Hermano = null;
                return raiz;
            }
            if (pNodo.Hijo == null)
            {
                ClaseNodo temp = new ClaseNodo();
                temp.Dato = pDato;
                pNodo.Hijo = temp;
                return temp;
            }
            else 
            {
                aux = pNodo.Hijo;
                while (aux.Hermano != null)
                {
                    aux = aux.Hermano;
                }
                ClaseNodo temp = new ClaseNodo();
                temp.Dato = pDato;
                aux.Hermano = temp;
                return temp;
            }
        }
        public void TransversalPreorden(ClaseNodo pNodo, string nombre)
        {
            if (pNodo == null)
            {
                return;
            }
            string e = "";
            string nombrearchivo = nombre + ".txt";
            for (int j = 0; j < i; j++)
            {
                Console.Write("-");
                e += "-";
                
            }
                Console.WriteLine(pNodo.Dato);
                StreamWriter archivo = File.AppendText(nombrearchivo);
                archivo.WriteLine(e + pNodo.Dato);
                archivo.Close();
            if (pNodo.Hijo != null)
            {
                i++;
                TransversalPreorden(pNodo.Hijo,nombre);
                i--;
            }
            if (pNodo.Hermano != null)
            {
                TransversalPreorden(pNodo.Hermano, nombre);
            }
        }
        public ClaseNodo Buscar(string pDato, ClaseNodo pNodo)
        {
            ClaseNodo encontrado = null;
            if (pNodo == null)
            {
                return encontrado;
            }
            if (pNodo.Dato.CompareTo(pDato) == 0)
            {
                encontrado = pNodo;
                return encontrado;
            }
            if (pNodo.Hijo != null)
            {
                encontrado = Buscar(pDato, pNodo.Hijo);
                if (encontrado != null)
                {
                    return encontrado;
                }
            }
            if (pNodo.Hermano != null)
            {
                encontrado = Buscar(pDato, pNodo.Hermano);
                if (encontrado != null)
                {
                    return encontrado;
                }
            }
            return encontrado;
        }

    }
}
